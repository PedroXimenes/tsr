# -*- coding: utf-8 -*-
"""
Spyder Editor

Este é um arquivo de script temporário.
"""

print("teste")
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2
import numpy as np
import pickle
from math import ceil, sqrt
import tensorflow as tf


with open('C:/TSR/labels.pickle', 'rb') as f:
    y_label = pickle.load(f)

with open('C:/TSR/data2.pickle', 'rb') as f:
    data2 = pickle.load(f)   

 
def convert_to_grid(x_input):
    N, H, W, C = x_input.shape
    grid_size = int(ceil(sqrt(N)))
    grid_height = H * grid_size + 1 * (grid_size - 1)
    grid_width = W * grid_size + 1 * (grid_size - 1)
    grid = np.zeros((grid_height, grid_width, C)) + 255
    next_idx = 0
    y0, y1 = 0, H
    for y in range(grid_size):
        x0, x1 = 0, W
        for x in range(grid_size):
            if next_idx < N:
                img = x_input[next_idx]
                low, high = np.min(img), np.max(img)
                grid[y0:y1, x0:x1] = 255.0 * (img - low) / (high - low)
                next_idx += 1
            x0 += W + 1
            x1 += W + 1
        y0 += H + 1
        y1 += H + 1

    return grid



x_train = data2['x_train'].transpose(0, 2, 3, 1)
y_train = data2['y_train']
y_val = data2['y_validation']
y_test = data2['y_test']

x_val = data2['x_validation']
x_test = data2['x_test']


example = x_train[:10,:,:,:]

#fig = plt.figure()
#grid = convert_to_grid(example)
#plt.imshow(grid.astype('uint8'), cmap='gray')
#plt.axis('off')
#plt.gcf().set_size_inches(15, 15)
#plt.title('Some examples of training data', fontsize=18)
#plt.show()
#plt.close()
#
model = tf.keras.models.load_model('C:/TSR/model_v4.h5')
#

img=cv2.imread('C:/TSR/70.jpg', cv2.IMREAD_COLOR)
###print(img)

#img = x_train[0,:,:,:]#/255
img = img/255

img=cv2.resize(img,(32,32))
        

predict = model.predict(img.reshape(1,32,32,3))        

predict_value = np.amax(predict)
        
predict_index = np.argmax(predict)
print(predict_index)
        
traffic_sign = y_label[predict_index]
print('Temos', predict_value*100, '% de certeza que essa placa é um', traffic_sign)


#def show_webcam(mirror=False):
#    cam = cv2.VideoCapture(0)
#    while True:
#        ret_val, img = cam.read()
#        #if mirror: 
#         #   img = cv2.flip(img, 1)
#        cv2.imshow('lulu', img)
#
#        if cv2.waitKey(1) == 27: 
#            break  # esc to quit
#    cv2.destroyAllWindows()
#
##
#def main():
#    show_webcam(mirror=True)
#
#
#if __name__ == '__main__':
#    main()