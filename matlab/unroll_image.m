function [image_matrix,image_unrolled] = unroll_image(image_struct)

[dummmy, size1] = size(image_struct);

for i = 1:size1
    m1 = image_struct(i).images;
    m1 = m1(:);
    image_matrix(i,:) = m1';
end

image_unrolled = image_matrix(:);
end

