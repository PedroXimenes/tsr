function [ data_json ] = Get_Json()
D = '/MATLAB Drive/TSR/images';
S1 = dir(fullfile(D,'*.json')); 

image_struct = struct('images',{});

for k = 1:numel(S1)
    file = fullfile(D,S1(k).name);
    fid = fopen(file);
    raw = fread(fid,inf);
    str = char(raw');
    fclose(fid);
    image_struct(k).images = JSON.parse(str);
  
end    
data_json= image_struct;

end