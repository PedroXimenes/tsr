clc; clear all; close all;

temp = fileread('train.txt');

[dummy, sz] = size(temp);

j = 1;
for i = 1:22:sz
    
    names(j,:) = temp(i:i+21); 
    xjson(j,:) = [names(j,:) '.json'];
    j = j + 1;
end

[nFiles,dummy] = size(names);

json_struct = struct('json',{});

for k = 1:nFiles
    %file = fullfile(D,S1(k).name);
    fid = fopen(xjson(k,:));
    raw = fread(fid,inf);
    str = char(raw');
    fclose(fid);
    json_struct(k).json = JSON.parse(str);
end

vectorize_y(json_struct);