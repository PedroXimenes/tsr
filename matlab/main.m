%% Machine Learning TSR - Traffic Signal Recognition

%% Initialization
clear ; close all; clc

%% Setup the parameters you will use for this exercise
input_layer_size  = 3240000;  % 20x20 Input Images of Digits
hidden_layer_size = 25;   % 25 hidden units
num_labels = 73;          % 10 labels, from 1 to 10   
                         

%% =========== Part 1: Loading and Visualizing Data =============

% Load Training Data
fprintf('Loading and Visualizing Data ...\n')

Xfile = matfile ('C:\Users\Pedro Ximenes\Documents\Machine Learning\TSR- Traffic Sign Recognition\images\imageData.mat');
LoadX = Xfile.X;

load ('y_Data.mat');

[xm, xu] = unroll_image(LoadX); 
%xm represents the image matrix (Number of images x Number of pixels)
%xu represents the unrolled image matrix 

m = size(xm, 1);

aux = xm(2,:);
aux = reshape(aux,[1800 1800]);
imshow(aux);

xu = double(xu);
xm = double(xm);
% fprintf('Program paused. Press enter to continue.\n');
% pause;

%% 

load('JsonData.mat');
y_struct = X1; 
y_vec = vectorize_y(y_struct);

%-teste lulu
%% Load the weights into variables Theta1 and Theta2
 load('initialWeights.mat');
 
%% ================= Part 3: Implement Predict =================
 
 pred = predict(theta1, theta2, xm);
% 
fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y_vec)) * 100);
% 
% fprintf('Program paused. Press enter to continue.\n');
% pause;
% 
% %  To give you an idea of the network's output, you can also run
% %  through the examples one at the a time to see what it is predicting.
% 
% %  Randomly permute examples
% rp = randperm(m);
% 
% for i = 1:m
%     % Display 
%     fprintf('\nDisplaying Example Image\n');
%     displayData(X(rp(i), :));
% 
%     pred = predict(Theta1, Theta2, X(rp(i),:));
%     fprintf('\nNeural Network Prediction: %d (digit %d)\n', pred, mod(pred, 10));
%     
%     % Pause with quit option
%     s = input('Paused - press enter to continue, q to exit:','s');
%     if s == 'q'
%       break
%     end
% end

