%% Inicialização dos parâmetros theta1 e theta2


nImages = 36588;
nPixels = 3240000;
nNeurons = 25;
nLabels = 73;
init_epsolon = 0.12;

theta1 = rand(nNeurons, (nPixels + 1)) * (2*init_epsolon) - init_epsolon;
theta2 = rand(nLabels, (nNeurons + 1)) * (2*init_epsolon) - init_epsolon;
save ('initialWeights.mat','theta1','theta2');