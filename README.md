TSR---Traffic-Sign-Recognition
---- Desenvolvimento de um sistema capaz de reconhecer placas de trânsito utilizando redes neurais ----

Todo o código é desenvolvido em Python, utilizando o TensorFlow. O dataset utilizado para o treinamento da rede é armazenado no Google Cloud Storage. Após a importação do dataset, utiliza-se uma rede neural convolucional para que o sistema seja capaz de aprender a reconhecer as placas.

É necessário, para rodar o código, permissão de acesso ao Cloud Storage em que o dataset está armazenado e instalar o TensorFlow (desnecessário caso utilize o Google CoLab).